# Calendar-AddressBook Deployment

The project is implementation [Radicale Server](https://radicale.org/v3.html#web) wrapped in docker container with nginx+letsencrypt container creation and docker-compose deployment

### The Story

Created as personal project for practice of docker docker-compose, container registry, CI and CD


### Deployment

- Create account on [docker hub](hub.docker.com) or any other container registry and setup container registry. I use [digitalocean](https://digitalocean.com) for most of my project and their prices are pretty fair compared to other cloud providers.
- Build containers
  - `calendar` container :
    - Radicale server uses `apache2-utils` to generate user authentication. To pass it to container we need to export `DOCKERKIT=1` environment variable and mount secret to be passed into container for it to work. I have created `access.txt` file that has password inside, thus **create  your own access.txt file to build container**
  - `nginx+certbot` container:
    - I am using deprecated container of cool group named [linuxserver](https://linuxserver.io), do give them a visit and contribute or donate, due to me being lazy, yet configuration works fine and certs are generated, thus should work fine. In case your are worried regarding the security, you are welcome to **contribute**.
```sh
docker build -f Dockerfile.cal -t calendar:0.0.0 . # dot is required
docker build -f Dockerfile.https -t https:0.0.0 . # docker is required
```
- Push containers to registry:
```sh
docker tag calendar:0.0.0  hub.docker.com/silent-mobius/cal:0.0.0
docker tag https:0.0.0  hub.docker.com/silent-mobius/https:0.0.0
docker push hub.docker.com/silent-mobius/cal:0.0.0
docker push hub.docker.com/silent-mobius/https:0.0.0
```
- Test with locally with  `docker-compose.yml` file
```sh
curl -L get.docker.com | sudo bash # incase you don't have docker installed
docker compose -f docker-compose.yml up -d
```
> Note: in case you are using private registry, you'll need to login with `docker login my-private-registry.com`


- Test remotely with `docker-compose.prod.yml` file
```sh
curl -L get.docker.com | sudo bash # incase you don't have docker installed
docker compose -f docker-compose.prod.yml up -d
```
> Note: in case you are using private registry, you'll need to login with `docker login my-private-registry.com`

### License

This project is under MIT License. **Radicale Server Comes With Its Own License Thus Please [Check It Out](https://github.com/Kozea/Radicale/tree/v3)**


### Notes:
- Do try to have fun
- Subscribe to my [dev.to](dev.to/silent_mobius) 
- Contact me if you need help with Linux(OS, Architecture, Network, Security, Automation), Shell-Script(Bash), Python(back-end), GoLang(back-end), CI (Jenkins, GitLab-CI, GitHub-Actions), CD, Config-Management(Ansible), IaC (Terraform, Vagrant), Containers (Docker, Containerd, Podman), Container-Orchestration (K8s, Nomad)